<?php require_once('server.php'); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Activity: S05</title>
</head>
<body>

<?php if (LogIn()): ?>
    <p>Hello, <?php echo ($_SESSION['username']); ?>!</p>
    <form method="post" action="index.php">
        <input type="submit" name="logout" value="Logout">
    </form>
<?php else: ?>
    <form method="post" action="index.php">
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" required>
        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required>
        <input type="submit" value="Login">
    </form>
    <?php if (isset($errorMessage)): ?>
        <p><?php echo ($errorMessage); ?></p>
    <?php endif; ?>
<?php endif; ?>

</body>
</html>

